//
//  Player.swift
//  Ratings
//
//  Created by Михаил Сахнов on 13.08.15.
//
//

import UIKit

class Player: NSObject {
    var name: String
    var game: String
    var rating: Int
    
    init(name: String, game: String, rating: Int) {
        self.name = name
        self.game = game
        self.rating = rating
        super.init()
    }
}