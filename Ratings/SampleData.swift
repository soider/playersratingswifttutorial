//
//  SampleData.swift
//  Ratings
//
//  Created by Михаил Сахнов on 13.08.15.
//
//

import Foundation


let playersData = [
    Player(name:"Bill Evans", game:"Tic-Tac-Toe", rating: 4),
    Player(name: "Oscar Peterson", game: "Spin the Bottle", rating: 5),
    Player(name: "Dave Brubeck", game: "Texas Hold 'em Poker", rating: 2)
]